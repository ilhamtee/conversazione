import 'package:firebase_chat_app/configs/style_config.dart';
import 'package:firebase_chat_app/helpers/global_helper.dart';
import 'package:firebase_chat_app/pages/auth/login_page.dart';
import 'package:firebase_chat_app/pages/home/home_page.dart';
import 'package:firebase_chat_app/pages/splash/splash_page.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main()async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  bool _isUserLoggedIn = false;
  @override
  void initState() {
    super.initState();
    _getUserLoggedInStatus();
  }

  void _getUserLoggedInStatus()async{
    await GlobalHelper.getUserLoggedInStatus().then((value) {
      if(value != null){
        setState(() {
          _isUserLoggedIn = value;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Chat App Firebase',
      theme: ThemeData(
        scaffoldBackgroundColor: Colors.white,
        primaryColor: StyleConfig.primaryColor,
      ),
      debugShowCheckedModeBanner: false,
      home: const SplashPage(),
    );
  }
}
