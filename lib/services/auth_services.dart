import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_chat_app/services/database_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthService {
  final firebaseAuth = FirebaseAuth.instance;

  // login
  Future signInUserToFirebase({required String email,
    required String password}) async {
    try {
      User? user = (await firebaseAuth.signInWithEmailAndPassword(
          email: email, password: password)).user;

      if(user != null){
        return true;
      }
    } on FirebaseAuthException catch (e) {
      debugPrint('Error Register $e');
      return e.message;
    }
  }

  // register
  Future registerUserToFirebase(
      {required String email,
      required String name,
      required String password}) async {
    try {
      User? user = (await firebaseAuth.createUserWithEmailAndPassword(
          email: email, password: password)).user;

      if(user != null){
        await DatabaseService(userId: user.uid).addUserData(name, email);
        return true;
      }
    } on FirebaseAuthException catch (e) {
      debugPrint('Error Register $e');
      return e.message;
    }
  }

  // sign out
  Future logoutUser()async{
    try{
      final sf = await SharedPreferences.getInstance();
      await sf.clear();
      await firebaseAuth.signOut();
    }catch(e){
      return null;
    }
  }
}
