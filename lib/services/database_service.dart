import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_chat_app/helpers/global_helper.dart';

class DatabaseService {
  final String? userId;
  DatabaseService({this.userId});

  // reference to firestore collection
  final CollectionReference userCollection =
      FirebaseFirestore.instance.collection('users');
  final CollectionReference groupCollection =
      FirebaseFirestore.instance.collection('groupsChat');

  // save user data to firestore
  Future addUserData(String fullName, String email) async {
    return await userCollection.doc(userId).set({
      'fullName': fullName,
      'email': email,
      'groups': [],
      'profilePic': '',
      'userId': userId
    });
  }

  // get logged user data
  Future<QuerySnapshot> getUserData({required String userEmail}) async {
    QuerySnapshot querySnapshot =
        await userCollection.where('email', isEqualTo: userEmail).get();

    return querySnapshot;
  }

  // get user groups
  Future<Stream> getUserGroup() async {
    return userCollection.doc(userId).snapshots();
  }

  Future createGroup(
      {required String userName,
      required String userId,
      required String groupName}) async {
    DocumentReference groupDocumentReference = await groupCollection.add({
      'groupName': groupName,
      'groupIcon': '',
      'admin': '${userId}_$userName',
      'members': [],
      'groupId': '',
      'recentMessage': '',
      'recentMessageSender': ''
    });

    // update the members of group
    await groupDocumentReference.update({
      'groupId': groupDocumentReference.id,
      'members': FieldValue.arrayUnion(['${userId}_$userName'])
    });

    // update list group of data user
    DocumentReference userDocumentReference = userCollection.doc(userId);
    return await userDocumentReference.update({
      'groups':
          FieldValue.arrayUnion(['${groupDocumentReference.id}_$groupName'])
    });
  }

  Future<Stream> getChats(String groupId) async {
    return groupCollection
        .doc(groupId)
        .collection('messages')
        .orderBy('time')
        .snapshots();
  }

  Future<String> getGroupAdmin(String groupId) async {
    DocumentReference groupDocumentReference = groupCollection.doc(groupId);

    final groupSnapshot = await groupDocumentReference.get();
    return groupSnapshot.get('admin');
  }

  Future<Stream<DocumentSnapshot>> getGroupMember(String groupId) async {
    return groupCollection.doc(groupId).snapshots();
  }

  Future searchGroupByName(String groupName) async {
    return groupCollection.where('groupName', isEqualTo: groupName).get();
  }

  Future<bool> isUserJoined(String groupId, String groupName) async {
    DocumentReference userDocumentReference = userCollection.doc(userId);
    DocumentSnapshot userSnapshot = await userDocumentReference.get();

    List<dynamic> userGroups = await userSnapshot['groups'];
    if (userGroups.contains('${groupId}_$groupName')) {
      return true;
    } else {
      return false;
    }
  }

  Future<String> checkGroupAdmin(String groupAdmin) async {
    DocumentReference userDocumentReference = userCollection.doc(userId);
    DocumentSnapshot userSnapshot = await userDocumentReference.get();

    String groupAdminName = GlobalHelper.getGroupName(groupAdmin);
    String userNameLogged = userSnapshot['fullName'];

    if (groupAdminName == userNameLogged) {
      return 'You';
    } else {
      return groupAdminName;
    }
  }

  Future handleJoinGroup(
      String groupId, String userName, String groupName) async {
    // doc reference
    DocumentReference userDocumentReference = userCollection.doc(userId);
    DocumentReference groupDocumentReference = groupCollection.doc(groupId);

    DocumentSnapshot userSnapshot = await userDocumentReference.get();
    List<dynamic> userGroups = await userSnapshot['groups'];

    // if user was join the group
    // then remove from group or also in other part re join
    if (userGroups.contains("${groupId}_$groupName")) {
      await userDocumentReference.update({
        "groups": FieldValue.arrayRemove(["${groupId}_$groupName"])
      });
      await groupDocumentReference.update({
        "members": FieldValue.arrayRemove(["${userId}_$userName"])
      });
    } else {
      await userDocumentReference.update({
        "groups": FieldValue.arrayUnion(["${groupId}_$groupName"])
      });
      await groupDocumentReference.update({
        "members": FieldValue.arrayUnion(["${userId}_$userName"])
      });
    }
  }

  void sendMessage(
      {required String groupId, required Map<String, dynamic> messageDataMap})async {
    await groupCollection.doc(groupId).collection("messages").add(messageDataMap);
    await groupCollection.doc(groupId).update({
      "recentMessage": messageDataMap['message'],
      "recentMessageSender": messageDataMap['sender'],
      "recentMessageTime": messageDataMap['time'].toString(),
    });
  }
}
