import 'package:firebase_chat_app/configs/style_config.dart';
import 'package:flutter/material.dart';

class GlobalWidget {
  static const textInputDecoration = InputDecoration(
    labelStyle: TextStyle(color: Colors.black, fontWeight: FontWeight.w400),
    focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(color: StyleConfig.primaryColor, width: 2)),
    enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(color: StyleConfig.primaryColor, width: 2)),
    errorBorder:
        OutlineInputBorder(borderSide: BorderSide(color: Colors.red, width: 2)),
  );

  static InputDecoration roundedTextInputDecoration = InputDecoration(
    labelStyle:
        const TextStyle(color: Colors.black, fontWeight: FontWeight.w400),
    contentPadding: const EdgeInsets.symmetric(horizontal: 20),
    focusedBorder: OutlineInputBorder(
        borderSide: const BorderSide(color: StyleConfig.primaryColor, width: 2),
        borderRadius: BorderRadius.circular(30)),
    enabledBorder: OutlineInputBorder(
        borderSide: const BorderSide(color: StyleConfig.primaryColor, width: 2),
        borderRadius: BorderRadius.circular(30)),
    errorBorder: OutlineInputBorder(
        borderSide: const BorderSide(color: Colors.red, width: 2),
        borderRadius: BorderRadius.circular(30)),
  );

  static void showSnackBar(
      {required BuildContext context,
      required Color backgroundColor,
      required String message}) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(
        message,
        style: const TextStyle(fontSize: 14),
      ),
      backgroundColor: backgroundColor,
      duration: const Duration(seconds: 3),
    ));
  }

  static void showSignOutBottomSheet(
      {required BuildContext context, required VoidCallback onYesFunction}) {
    showModalBottomSheet<void>(
        context: context,
        builder: (BuildContext context) {
          return SizedBox(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Image.asset(
                  'assets/images/confirmation.jpg',
                  width: 300,
                  height: 300,
                  fit: BoxFit.contain,
                ),
                const Text(
                  'Are you sure want to log out?',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 17,
                      fontWeight: FontWeight.w400),
                ),
                Container(
                  alignment: Alignment.bottomCenter,
                  padding:
                      const EdgeInsets.only(left: 60, right: 60, bottom: 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Expanded(
                          child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                  backgroundColor: StyleConfig.primaryColor),
                              onPressed: onYesFunction,
                              child: const Text(
                                'YES',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 17,
                                    fontWeight: FontWeight.bold),
                              ))),
                      const SizedBox(width: 15),
                      Expanded(
                          child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                  backgroundColor: StyleConfig.primaryColor),
                              onPressed: () => Navigator.pop(context),
                              child: const Text(
                                'CANCEL',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 17,
                                    fontWeight: FontWeight.bold),
                              ))),
                    ],
                  ),
                )
              ],
            ),
          );
        });
  }

  static Widget listTileDrawer(
      {required String name,
      required IconData icon,
      required VoidCallback onTapFunction,
      bool isSelected = false}) {
    return ListTile(
      onTap: onTapFunction,
      selectedColor: StyleConfig.primaryColor,
      selected: isSelected,
      contentPadding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      leading: Icon(icon),
      title: Text(
        name,
        style: const TextStyle(color: Colors.black),
      ),
    );
  }
}
