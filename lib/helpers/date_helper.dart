import 'package:intl/intl.dart';

class DateTimeHelper {
  static String timeMessageFromTimeStamp(String timeStamp) {
    String resultTime = '';

    final dateNow = DateTime.now();
    final messageTime =
        DateTime.fromMillisecondsSinceEpoch(int.parse(timeStamp));
    final differentTime = messageTime.difference(dateNow);

    if((differentTime.inHours * -1) < 12 && differentTime.inDays == 0){
      return 'Today ${DateFormat('HH:mm').format(messageTime)}';
    }else if((differentTime.inHours * -1) >= 12){
      return 'Yesterday ${DateFormat('HH:mm').format(messageTime)}';
    }else if(differentTime.inDays > 1 && differentTime.inDays < 7){
      return DateFormat('EEEE HH:mm').format(messageTime);
    }else{
      return DateFormat('EE, dd MMMM yyyy  HH:mm').format(messageTime);
    }

    return resultTime;
  }
}
