import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';

class GlobalHelper {
  //keys
  static const String userLoggedKey = 'LOGGEDKEY';
  static const String userNameKey = 'USERNAMEKEY';
  static const String userEmailKey = 'USEREMAILKEY';

  //save data to Shared Preferences
  static Future<bool?> saveUserLoggedInData(
      {required bool isUserLoggedIn,
      required String userName,
      required String userEmail}) async {
    final sf = await SharedPreferences.getInstance();
    await sf.setString(userNameKey, userName);
    await sf.setString(userEmailKey, userEmail);

    return await sf.setBool(userLoggedKey, isUserLoggedIn);
  }

  //get data from Shared Preferences
  static Future<bool?> getUserLoggedInStatus() async {
    final sharedPreferences = await SharedPreferences.getInstance();

    return sharedPreferences.getBool(userLoggedKey);
  }

  static void goToNextScreen(context, page) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => page));
  }

  static void goToReplacementScreen(context, page) {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (context) => page),
            (route) => false
    );
  }
  
  static String getGroupId(String value){
    return value.substring(0, value.indexOf('_'));
  }
  
  static String getGroupName(String value){
    return value.substring(value.indexOf('_') + 1);
  }

  static String getName(String value){
    return value.substring(value.indexOf('_') + 1);
  }
}
