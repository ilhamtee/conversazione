import 'package:firebase_chat_app/helpers/global_helper.dart';
import 'package:firebase_chat_app/pages/auth/login_page.dart';
import 'package:firebase_chat_app/pages/home/home_page.dart';
import 'package:flutter/material.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    _getUserLoggedInStatus();
    super.initState();
  }

  void _getUserLoggedInStatus()async{
    Future.delayed(const Duration(milliseconds: 1500),()async{
      await GlobalHelper.getUserLoggedInStatus().then((value) {
        if(value == true){
          GlobalHelper.goToReplacementScreen(context, const HomePage());
        }else{
          GlobalHelper.goToReplacementScreen(context, const LoginPage());
        }
      });
    });

  }


  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      child: Center(
        child: Image.asset('assets/images/splash.jpg', width: 300,),
      ),
    );
  }
}
