import 'package:firebase_chat_app/configs/style_config.dart';
import 'package:firebase_chat_app/helpers/date_helper.dart';
import 'package:flutter/material.dart';

class ItemMessage extends StatelessWidget {
  final String message;
  final String sender;
  final bool isSentByMe;
  final String dateMessage;
  const ItemMessage(
      {Key? key,
      required this.message,
      required this.sender,
      required this.isSentByMe,
      required this.dateMessage})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: isSentByMe ? Alignment.centerRight : Alignment.centerLeft,
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 15),
        margin: EdgeInsets.only(left: isSentByMe ? 100 : 0),
        decoration: BoxDecoration(
            color: isSentByMe ? StyleConfig.primaryColor : Colors.grey[700],
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(15),
                topRight: Radius.circular(15),
                bottomLeft:
                    isSentByMe ? Radius.circular(15) : Radius.circular(0),
                bottomRight:
                    isSentByMe ? Radius.circular(0) : Radius.circular(15))),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(sender,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 14,
                    fontWeight: FontWeight.w500),
                maxLines: 1,
                overflow: TextOverflow.ellipsis),
            SizedBox(height: 10),
            Text(
              message,
              style: TextStyle(color: Colors.white),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              DateTimeHelper.timeMessageFromTimeStamp(dateMessage),
              style: TextStyle(color: isSentByMe
                  ? Colors.grey[800]
                  : Colors.white, fontSize: 12),
            ),
          ],
        ),
      ),
    );
  }
}
