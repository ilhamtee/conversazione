import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_chat_app/configs/style_config.dart';
import 'package:firebase_chat_app/helpers/global_helper.dart';
import 'package:firebase_chat_app/helpers/global_widget.dart';
import 'package:firebase_chat_app/pages/home/home_page.dart';
import 'package:firebase_chat_app/services/database_service.dart';
import 'package:flutter/material.dart';

class GroupInfoPage extends StatelessWidget {
  final String groupName;
  final String groupId;
  final String groupAdmin;
  final String userName;
  final Stream<DocumentSnapshot> listGroupMember;
  const GroupInfoPage(
      {Key? key,
      required this.groupName,
      required this.groupId,
      required this.groupAdmin,
      required this.listGroupMember,
      required this.userName})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: StyleConfig.primaryColor,
        title: const Text('Group Info'),
        centerTitle: true,
        elevation: 0,
        actions: [
          IconButton(
              onPressed: () async {
                showDialog(
                    context: context,
                    builder: (context) {
                      return AlertDialog(
                        title: const Text('Leave Group'),
                        contentPadding: const EdgeInsets.symmetric(
                            vertical: 20, horizontal: 20),
                        content: const Text(
                          'Are you sure want to leave the group?',
                          style: TextStyle(fontSize: 18),
                        ),
                        actions: [
                          GestureDetector(
                            onTap: () => Navigator.pop(context),
                            child: const Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 5, vertical: 5),
                              child: Text(
                                'NO',
                                style: TextStyle(
                                    color: StyleConfig.primaryColor,
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500),
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              DatabaseService(
                                      userId: FirebaseAuth
                                          .instance.currentUser!.uid)
                                  .handleJoinGroup(groupId, userName, groupName)
                                  .whenComplete(() {
                                GlobalWidget.showSnackBar(
                                    context: context,
                                    backgroundColor: Colors.red,
                                    message: 'You have left the $groupName '
                                        'group');
                                GlobalHelper.goToReplacementScreen(
                                    context, const HomePage());
                              });
                            },
                            child: const Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 5, vertical: 5),
                              child: Text(
                                'YES',
                                style: TextStyle(
                                    color: StyleConfig.primaryColor,
                                    fontSize: 16,
                                    fontWeight: FontWeight.w400),
                              ),
                            ),
                          ),
                        ],
                      );
                    });
              },
              icon: const Icon(Icons.exit_to_app))
        ],
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        child: Column(
          children: [
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 20),
              decoration: BoxDecoration(
                  borderRadius: const BorderRadius.all(Radius.circular(20)),
                  color: StyleConfig.primaryColor.withOpacity(0.5)),
              child: ListTile(
                leading: CircleAvatar(
                  backgroundColor: StyleConfig.primaryColor,
                  radius: 30,
                  child: Text(
                    groupName.substring(0, 1).toUpperCase(),
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                        fontWeight: FontWeight.w500,
                        color: Colors.white,
                        fontSize: 20),
                  ),
                ),
                title: Text(
                  'Group : $groupName',
                  style: const TextStyle(
                      fontWeight: FontWeight.w400, fontSize: 18),
                ),
                subtitle: Text(
                  'Admin : ${GlobalHelper.getName(groupAdmin)}',
                  style: const TextStyle(fontSize: 17),
                ),
              ),
            ),
            StreamBuilder(
                stream: listGroupMember,
                builder: (context, AsyncSnapshot snapshot) {
                  if (snapshot.hasData) {
                    final listMember = snapshot.data['members'];
                    if (listMember != null) {
                      if (listMember.length > 0) {
                        return ListView.builder(
                            shrinkWrap: true,
                            itemCount: listMember.length,
                            itemBuilder: (context, index) {
                              return Container(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 10, vertical: 20),
                                child: ListTile(
                                  leading: CircleAvatar(
                                    backgroundColor: StyleConfig.primaryColor,
                                    radius: 30,
                                    child: Text(
                                      GlobalHelper.getGroupName(
                                              listMember[index])
                                          .substring(0, 1)
                                          .toUpperCase(),
                                      textAlign: TextAlign.center,
                                      style: const TextStyle(
                                          fontWeight: FontWeight.w500,
                                          color: Colors.white,
                                          fontSize: 20),
                                    ),
                                  ),
                                  title: Text(
                                    GlobalHelper.getGroupName(
                                        listMember[index]),
                                    style: const TextStyle(
                                        fontWeight: FontWeight.w400,
                                        fontSize: 18),
                                  ),
                                  subtitle: Text(
                                    groupId,
                                    style: const TextStyle(fontSize: 17),
                                  ),
                                ),
                              );
                            });
                      } else {
                        return Center(
                          child: Text('This Group Has No Member'),
                        );
                      }
                    } else {
                      return Center(
                        child: Text('This Group Has No Member'),
                      );
                    }
                  } else {
                    return const Center(
                      child: CircularProgressIndicator(
                        backgroundColor: StyleConfig.primaryColor,
                        color: Colors.white,
                      ),
                    );
                  }
                })
          ],
        ),
      ),
    );
  }
}
