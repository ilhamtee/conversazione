import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_chat_app/configs/style_config.dart';
import 'package:firebase_chat_app/helpers/global_helper.dart';
import 'package:firebase_chat_app/pages/chat/group_info_page.dart';
import 'package:firebase_chat_app/pages/chat/widgets/item_message.dart';
import 'package:firebase_chat_app/services/database_service.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ChatPage extends StatefulWidget {
  final String groupId;
  final String groupName;
  final String userName;
  const ChatPage(
      {Key? key,
      required this.groupId,
      required this.groupName,
      required this.userName})
      : super(key: key);

  @override
  State<ChatPage> createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  String _groupAdmin = '';
  String _userName = '';

  Stream<QuerySnapshot>? _getChats;
  Stream<DocumentSnapshot>? _listGroupMember;



  @override
  void initState() {
    super.initState();
    _getGroupMember();
    _getChatAndGroupAdmin();
  }

  void _getChatAndGroupAdmin() async {
    DatabaseService().getChats(widget.groupId).then((chats) {
      _getChats = chats as Stream<QuerySnapshot<Object?>>?;
    });
    DatabaseService().getGroupAdmin(widget.groupId).then((admin) {
      _groupAdmin = admin;
    });
    final sf = await SharedPreferences.getInstance();
    _userName = sf.getString(GlobalHelper.userNameKey) ?? '';
    setState(() {});
  }

  void _getGroupMember() {
    DatabaseService(userId: FirebaseAuth.instance.currentUser!.uid)
        .getGroupMember(widget.groupId)
        .then((value) {
      _listGroupMember = value;
    });
  }

  final TextEditingController _chatController = TextEditingController();
  ScrollController listScrollController = ScrollController();

  @override
  void dispose() {
    super.dispose();
    _chatController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: StyleConfig.primaryColor,
        title: Text(widget.groupName),
        centerTitle: true,
        elevation: 0,
        actions: [
          IconButton(
              onPressed: () => GlobalHelper.goToNextScreen(
                  context,
                  GroupInfoPage(
                    groupName: widget.groupName,
                    groupId: widget.groupId,
                    groupAdmin: _groupAdmin,
                    userName: _userName,
                    listGroupMember: _listGroupMember!,
                  )),
              icon: const Icon(Icons.info))
        ],
      ),
      body: Stack(
        children: [
          StreamBuilder(
            stream: _getChats,
            builder: (context, AsyncSnapshot snapshot) {
              WidgetsBinding.instance.addPostFrameCallback((_) {
                if (listScrollController.hasClients) {
                  final position = listScrollController.position.maxScrollExtent;
                  listScrollController.animateTo(
                    position,
                    duration: Duration(milliseconds: 500),
                    curve: Curves.easeOut,
                  );
                } else {
                  setState(() => null);
                }
              });
              if (snapshot.hasData) {
                if (snapshot.data.docs.length > 0) {
                  return ListView.builder(
                      controller: listScrollController,
                      itemCount: snapshot.data.docs.length,
                      shrinkWrap: true,
                      padding: EdgeInsets.only(
                          bottom: 100),
                      itemBuilder: (context, index) {
                        final messageData = snapshot.data.docs[index];
                        return ItemMessage(
                            message: messageData['message'],
                            sender: messageData['sender'],
                            isSentByMe: messageData['sender'] == _userName,
                            dateMessage: messageData['time']);
                      });
                } else {
                  return Center(
                      child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        'assets/images/no_chat_group.png',
                        width: 350,
                        fit: BoxFit.fill,
                      ),
                      Text(
                        "There is no chat in this group yet \nLet's invite other members to have fun",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontStyle: FontStyle.italic, fontSize: 17),
                      ),
                    ],
                  ));
                }
              } else {
                return const Center(
                  child: CircularProgressIndicator(
                    color: Colors.white,
                    backgroundColor: StyleConfig.primaryColor,
                  ),
                );
              }
            },
          ),
          Container(
            width: double.infinity,
            alignment: Alignment.bottomCenter,
            child: Container(
              margin: const EdgeInsets.symmetric(vertical: 15, horizontal: 15),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Expanded(
                      child: Container(
                    padding:
                        const EdgeInsets.symmetric(vertical: 5, horizontal: 20),
                    decoration: BoxDecoration(
                        color: Colors.grey[700],
                        borderRadius:
                            const BorderRadius.all(Radius.circular(30))),
                    child: TextFormField(
                      controller: _chatController,
                      cursorColor: StyleConfig.primaryColor,
                      maxLines: null,
                      minLines: 1,
                      style: const TextStyle(color: Colors.white),
                      decoration: const InputDecoration(
                          border: InputBorder.none,
                          hintText: 'Enter a message here ...',
                          hintStyle: TextStyle(color: Colors.white)),
                    ),
                  )),
                  const SizedBox(width: 10),
                  GestureDetector(
                    onTap: () {
                      if (_chatController.text.isNotEmpty) {
                        Map<String, dynamic> chatMessageMap = {
                          'message': _chatController.text,
                          'sender': _userName,
                          'time':
                              DateTime.now().millisecondsSinceEpoch.toString()
                        };

                        DatabaseService().sendMessage(
                            groupId: widget.groupId,
                            messageDataMap: chatMessageMap);
                        setState(() {
                          _chatController.clear();
                        });
                      }
                    },
                    child: Container(
                      width: 60,
                      height: 60,
                      decoration: const BoxDecoration(
                          shape: BoxShape.circle,
                          color: StyleConfig.primaryColor),
                      alignment: Alignment.center,
                      child: const Icon(
                        Icons.send,
                        color: Colors.white,
                        size: 30,
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
