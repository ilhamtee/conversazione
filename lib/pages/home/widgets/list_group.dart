import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_chat_app/configs/style_config.dart';
import 'package:firebase_chat_app/helpers/global_helper.dart';
import 'package:firebase_chat_app/pages/home/widgets/item_group.dart';
import 'package:firebase_chat_app/pages/home/widgets/no_group_widget.dart';
import 'package:firebase_chat_app/services/database_service.dart';
import 'package:flutter/material.dart';

class ListGroup extends StatefulWidget {
  const ListGroup({Key? key}) : super(key: key);

  @override
  State<ListGroup> createState() => _ListGroupState();
}

class _ListGroupState extends State<ListGroup> {
  Stream? _getGroups;

  @override
  void initState() {
    super.initState();
    getDataGroups();
  }

  void getDataGroups() async {
    await DatabaseService(userId: FirebaseAuth.instance.currentUser!.uid)
        .getUserGroup()
        .then((snapshot) {
      setState(() {
        _getGroups = snapshot;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: StreamBuilder(
          stream: _getGroups, builder: (context, AsyncSnapshot snapshot) {
            if(snapshot.hasData){
              final dataGroups = snapshot.data['groups'];
              if(dataGroups != null){
                if(dataGroups.length > 0){
                  return ListView.builder(
                      itemCount: dataGroups.length,
                      shrinkWrap: true,
                      reverse: true,
                      itemBuilder: (context, index){
                        return ItemGroup(
                          groupId: GlobalHelper.getGroupId(dataGroups[index]),
                          groupName: GlobalHelper.getGroupName(dataGroups[index]),
                          userName: snapshot.data['fullName'],);
                      });
                }else{
                  return const NoGroupWidget();
                }
              }else{
                return const NoGroupWidget();
              }
            }else{
                return const Center(child: CircularProgressIndicator(
                  backgroundColor: StyleConfig.primaryColor,
                  color: Colors.white,
                ),);
            }
      }),
    );
  }
}
