import 'package:firebase_chat_app/configs/style_config.dart';
import 'package:flutter/material.dart';

class ListTileDrawer extends StatelessWidget {
  final String name;
  final IconData icon;
  final VoidCallback onTapFunction;
  final bool isSelected;

  const ListTileDrawer(
      {Key? key,
      required this.name,
      required this.icon,
      required this.onTapFunction, this.isSelected = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTapFunction,
      selectedColor: StyleConfig.primaryColor,
      selected: isSelected,
      contentPadding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      leading: Icon(icon),
      title: Text(
        name,
        style: const TextStyle(color: Colors.black),
      ),
    );
  }
}
