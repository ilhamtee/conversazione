import 'package:flutter/material.dart';

class NoGroupWidget extends StatelessWidget {
  const NoGroupWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 150),
      alignment: Alignment.center,
      child: Column(
        children: [
          Image.asset(
            'assets/images/empty_chat.jpg',
            height: 300,
            fit: BoxFit.fill,
          ),
          const SizedBox(height: 10),
          const Text(
            "Oops, you don't have a group chat yet. \nLet's create your own "
            "group chat or join with others group",
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 17
            ),
          )
        ],
      ),
    );
  }
}
