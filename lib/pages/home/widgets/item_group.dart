import 'package:firebase_chat_app/configs/style_config.dart';
import 'package:firebase_chat_app/helpers/global_helper.dart';
import 'package:firebase_chat_app/pages/chat/chat_page.dart';
import 'package:flutter/material.dart';

class ItemGroup extends StatelessWidget {
  final String userName;
  final String groupId;
  final String groupName;
  const ItemGroup(
      {Key? key,
      required this.userName,
      required this.groupId,
      required this.groupName})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      contentPadding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      onTap: () => GlobalHelper.goToNextScreen(context,
          ChatPage(groupId: groupId,
              groupName: groupName,
              userName: userName)),
      leading: CircleAvatar(
        radius: 30,
        backgroundColor: StyleConfig.primaryColor,
        child: Text(
          groupName.substring(0, 1).toUpperCase(),
          textAlign: TextAlign.center,
          style:
              const TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
        ),
      ),
      title: Text(
        groupName,
        style: const TextStyle(
            color: Colors.black, fontWeight: FontWeight.w500, fontSize: 17),
      ),
      subtitle: Text('$userName has join $groupName group'),
    );
  }
}
