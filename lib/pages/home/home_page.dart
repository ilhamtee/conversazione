import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_chat_app/configs/style_config.dart';
import 'package:firebase_chat_app/helpers/global_helper.dart';
import 'package:firebase_chat_app/helpers/global_widget.dart';
import 'package:firebase_chat_app/pages/auth/login_page.dart';
import 'package:firebase_chat_app/pages/home/widgets/list_group.dart';
import 'package:firebase_chat_app/pages/home/widgets/list_tile_drawer.dart';
import 'package:firebase_chat_app/pages/profile/profile_page.dart';
import 'package:firebase_chat_app/pages/search/search_page.dart';
import 'package:firebase_chat_app/services/auth_services.dart';
import 'package:firebase_chat_app/services/database_service.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final AuthService _authService = AuthService();

  // Data user
  String _userName = '';
  String _userEmail = '';

  // Data home page
  int _selectedPage = 0;

  bool _isLoadingCreateGroup = false;
  final TextEditingController _groupNameController = TextEditingController();

  @override
  void initState() {
    _getDataUser();
    super.initState();
  }

  Future _getDataUser() async {
    SharedPreferences sf = await SharedPreferences.getInstance();
    setState(() {
      _userName = sf.getString(GlobalHelper.userNameKey) ?? '';
      _userEmail = sf.getString(GlobalHelper.userEmailKey) ?? '';
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: StyleConfig.primaryColor,
        actions: [
          IconButton(
              onPressed: () =>
                  GlobalHelper.goToNextScreen(context, const SearchPage()),
              icon: const Icon(
                Icons.search,
                size: 27,
              ))
        ],
        elevation: 0,
        title: Text(
          _selectedPage == 0 ? 'Groups Chat' : 'Profile',
          style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
      ),
      body: _selectedPage == 0
          ? const ListGroup()
          : ProfilePage(fullName: _userName, email: _userEmail) ,
      drawer: Drawer(
        elevation: 3,
        child: ListView(
          padding: const EdgeInsets.symmetric(vertical: 50),
          children: [
            const Icon(
              Icons.account_circle,
              size: 150,
              color: StyleConfig.primaryColor,
            ),
            const SizedBox(height: 10),
            Text(
              _userName,
              textAlign: TextAlign.center,
              style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
            ),
            const SizedBox(height: 30),
            const Divider(
              height: 2,
            ),
            ListTileDrawer(
                name: 'Group',
                icon: Icons.group,
                isSelected: _selectedPage == 0,
                onTapFunction: () {
                  Navigator.pop(context);
                  if (_selectedPage != 0) {
                    setState(() {
                      _selectedPage = 0;
                    });
                  }
                }),
            ListTileDrawer(
                name: 'Profile',
                icon: Icons.person,
                isSelected: _selectedPage == 1,
                onTapFunction: () {
                  Navigator.pop(context);
                  if (_selectedPage != 1) {
                    setState(() {
                      _selectedPage = 1;
                    });
                  }
                }),
            ListTileDrawer(
                name: 'Logout',
                icon: Icons.exit_to_app,
                isSelected: false,
                onTapFunction: () {
                  GlobalWidget.showSignOutBottomSheet(
                      context: context,
                      onYesFunction: () {
                        _authService.logoutUser();
                        GlobalHelper.goToReplacementScreen(
                            context, const LoginPage());
                      });
                })
          ],
        ),
      ),
      floatingActionButton: Container(
        width: 80,
        height: 80,
        margin: const EdgeInsets.only(right: 10, bottom: 30),
        child: FittedBox(
          child: FloatingActionButton(
            onPressed: () {
              showDialog(
                  context: context,
                  barrierDismissible: false,
                  builder: (context) {
                    return StatefulBuilder(builder: (context, setState) {
                      return AlertDialog(
                        title: const Text('Create A Group'),
                        content: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            _isLoadingCreateGroup
                                ? const CircularProgressIndicator(
                                    backgroundColor: StyleConfig.primaryColor,
                                    color: Colors.white,
                                  )
                                : TextField(
                                    controller: _groupNameController,
                                    decoration: GlobalWidget
                                        .roundedTextInputDecoration
                                        .copyWith(
                                            labelText: 'Enter a group name'),
                                  )
                          ],
                        ),
                        actions: [
                          ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                  backgroundColor: StyleConfig.primaryColor),
                              onPressed: () {
                                if(!_isLoadingCreateGroup){
                                  Navigator.pop(context);
                                }
                              },
                              child: const Text('CANCEL')),
                          ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                  backgroundColor: StyleConfig.primaryColor),
                              onPressed: ()async {
                                if (_groupNameController.text.isNotEmpty &&
                                    !_isLoadingCreateGroup) {
                                  setState(() {
                                    _isLoadingCreateGroup = true;
                                  });
                                  await Future.delayed(
                                      const Duration(milliseconds: 1500));
                                  DatabaseService(
                                          userId: FirebaseAuth
                                              .instance.currentUser!.uid)
                                      .createGroup(
                                          userName: _userName,
                                          userId: FirebaseAuth
                                              .instance.currentUser!.uid,
                                          groupName: _groupNameController.text)
                                      .whenComplete(() {
                                    _isLoadingCreateGroup = false;
                                    _groupNameController.text = '';
                                  });
                                  Navigator.pop(context);
                                  GlobalWidget.showSnackBar(
                                      context: context,
                                      backgroundColor: Colors.green,
                                      message: 'Success Create a Group');
                                }
                              },
                              child: const Text('CREATE'))
                        ],
                      );
                    });
                  });
            },
            backgroundColor: StyleConfig.primaryColor,
            child: const Icon(
              Icons.chat,
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }
}
