import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_chat_app/configs/style_config.dart';
import 'package:firebase_chat_app/helpers/global_helper.dart';
import 'package:firebase_chat_app/helpers/global_widget.dart';
import 'package:firebase_chat_app/pages/auth/register_page.dart';
import 'package:firebase_chat_app/pages/home/home_page.dart';
import 'package:firebase_chat_app/services/auth_services.dart';
import 'package:firebase_chat_app/services/database_service.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _formKey = GlobalKey<FormState>();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  bool _isLoadingLogin = false;
  final AuthService _authService = AuthService();

  @override
  void dispose() {
    super.dispose();
    _emailController.dispose();
    _passwordController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _isLoadingLogin
          ? const Center(
              child: CircularProgressIndicator(color: StyleConfig.primaryColor),
            )
          : SingleChildScrollView(
              padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 80),
              child: Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Text(
                      'CONVERSAZIONE',
                      style: TextStyle(
                          fontSize: 30,
                          fontWeight: FontWeight.bold,
                          fontStyle: FontStyle.italic),
                    ),
                    const SizedBox(height: 5),
                    const Text(
                      'Want to enjoy the fun of chatting? Login Now!',
                      style: TextStyle(color: Colors.black),
                    ),
                    const SizedBox(height: 50),
                    Center(
                      child: Image.asset(
                        'assets/images/login_bg.jpg',
                        width: MediaQuery.of(context).size.width * 0.8,
                        height: MediaQuery.of(context).size.width * 0.35,
                      ),
                    ),
                    TextFormField(
                      controller: _emailController,
                      keyboardType: TextInputType.emailAddress,
                      decoration: GlobalWidget.textInputDecoration.copyWith(
                          labelText: 'Email',
                          prefixIcon: const Icon(
                            Icons.email,
                            color: StyleConfig.primaryColor,
                          )),
                      validator: (value) {
                        return RegExp(
                                    r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
                                .hasMatch(value!)
                            ? null
                            : 'Please enter a valid email';
                      },
                    ),
                    const SizedBox(height: 15),
                    TextFormField(
                      controller: _passwordController,
                      obscureText: true,
                      decoration: GlobalWidget.textInputDecoration.copyWith(
                          labelText: 'Password',
                          prefixIcon: const Icon(
                            Icons.lock,
                            color: StyleConfig.primaryColor,
                          )),
                      validator: (value) {
                        if (value!.length < 6)
                          return 'Password must be a least 6 characters';

                        return null;
                      },
                    ),
                    const SizedBox(height: 20),
                    SizedBox(
                        width: double.infinity,
                        height: 45,
                        child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                backgroundColor: StyleConfig.primaryColor,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20))),
                            onPressed: () => doLogin(),
                            child: const Text(
                              'LOGIN',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 17,
                                  fontWeight: FontWeight.bold),
                            ))),
                    SizedBox(height: 10),
                    Text.rich(TextSpan(
                        text: "Don't have an account?",
                        style: TextStyle(color: Colors.black, fontSize: 14),
                        children: <TextSpan>[
                          TextSpan(
                              text: ' Register here',
                              style: TextStyle(
                                  color: StyleConfig.primaryColor,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 14),
                              recognizer: TapGestureRecognizer()
                                ..onTap = () {
                                  GlobalHelper.goToNextScreen(
                                      context, const RegisterPage());
                                })
                        ]))
                  ],
                ),
              ),
            ),
    );
  }

  void doLogin() async {
    if (_formKey.currentState!.validate()) {
      setState(() {
        _isLoadingLogin = true;
      });

      await _authService
          .signInUserToFirebase(
              email: _emailController.text,
              password: _passwordController.text)
          .then((value) async {
        if (value == true) {
          final resultUserData = await DatabaseService(userId: FirebaseAuth.instance.currentUser!.uid)
              .getUserData(userEmail: _emailController.text);

          await GlobalHelper.saveUserLoggedInData(
              isUserLoggedIn: true,
              userName: resultUserData.docs.first['fullName'],
              userEmail: _emailController.text);

          GlobalHelper.goToReplacementScreen(context,const HomePage());
        } else {
          setState(() {
            _isLoadingLogin = false;
          });
          GlobalWidget.showSnackBar(
              context: context,
              backgroundColor: Colors.red,
              message: value.toString());
        }
      });
    }
  }
}
