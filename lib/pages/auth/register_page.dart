import 'package:firebase_chat_app/configs/style_config.dart';
import 'package:firebase_chat_app/helpers/global_helper.dart';
import 'package:firebase_chat_app/helpers/global_widget.dart';
import 'package:firebase_chat_app/pages/auth/login_page.dart';
import 'package:firebase_chat_app/pages/home/home_page.dart';
import 'package:firebase_chat_app/services/auth_services.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final _formKey = GlobalKey<FormState>();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  final _fullNameController = TextEditingController();

  bool _isLoadingRegister = false;
  final AuthService _authService = AuthService();

  @override
  void dispose() {
    super.dispose();
    _emailController.dispose();
    _fullNameController.dispose();
    _passwordController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _isLoadingRegister
          ? const Center(
              child: CircularProgressIndicator(color: StyleConfig.primaryColor),
            )
          : SingleChildScrollView(
              padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 80),
              child: Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Text(
                      'CONVERSAZIONE',
                      style: TextStyle(
                          fontSize: 30,
                          fontWeight: FontWeight.bold,
                          fontStyle: FontStyle.italic),
                    ),
                    const SizedBox(height: 5),
                    const Text(
                      'Create an account now for explore fun chat!',
                      style: TextStyle(color: Colors.black),
                    ),
                    const SizedBox(height: 50),
                    Center(
                      child: Image.asset(
                        'assets/images/register_bg.jpg',
                        width: MediaQuery.of(context).size.width * 0.8,
                        height: MediaQuery.of(context).size.width * 0.35,
                      ),
                    ),
                    const SizedBox(height: 15),
                    TextFormField(
                      controller: _fullNameController,
                      decoration: GlobalWidget.textInputDecoration.copyWith(
                          labelText: 'Full name',
                          prefixIcon: const Icon(
                            Icons.person,
                            color: StyleConfig.primaryColor,
                          )),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Name cannot be empty';
                        } else {
                          return null;
                        }
                      },
                    ),
                    const SizedBox(height: 15),
                    TextFormField(
                      controller: _emailController,
                      keyboardType: TextInputType.emailAddress,
                      decoration: GlobalWidget.textInputDecoration.copyWith(
                          labelText: 'Email',
                          prefixIcon: const Icon(
                            Icons.email,
                            color: StyleConfig.primaryColor,
                          )),
                      validator: (value) {
                        return RegExp(
                                    r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
                                .hasMatch(value!)
                            ? null
                            : 'Please enter a valid email';
                      },
                    ),
                    const SizedBox(height: 15),
                    TextFormField(
                      controller: _passwordController,
                      obscureText: true,
                      decoration: GlobalWidget.textInputDecoration.copyWith(
                          labelText: 'Password',
                          prefixIcon: const Icon(
                            Icons.lock,
                            color: StyleConfig.primaryColor,
                          )),
                      validator: (value) {
                        if (value!.length < 6)
                          return 'Password must be a least 6 characters';

                        return null;
                      },
                    ),
                    const SizedBox(height: 20),
                    SizedBox(
                        width: double.infinity,
                        height: 45,
                        child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                backgroundColor: StyleConfig.primaryColor,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20))),
                            onPressed: () => doRegister(),
                            child: const Text(
                              'REGISTER',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 17,
                                  fontWeight: FontWeight.bold),
                            ))),
                    const SizedBox(height: 10),
                    Text.rich(TextSpan(
                        text: "Already have an account?",
                        style:
                            const TextStyle(color: Colors.black, fontSize: 14),
                        children: <TextSpan>[
                          TextSpan(
                              text: ' Login now',
                              style: const TextStyle(
                                  color: StyleConfig.primaryColor,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 14),
                              recognizer: TapGestureRecognizer()
                                ..onTap = () {
                                  Navigator.pop(context);
                                })
                        ]))
                  ],
                ),
              ),
            ),
    );
  }

  void doRegister() async {
    if (_formKey.currentState!.validate()) {
      setState(() {
        _isLoadingRegister = true;
      });

      await _authService
          .registerUserToFirebase(
              email: _emailController.text,
              name: _fullNameController.text,
              password: _passwordController.text)
          .then((value) async{
        if (value == true) {
          await GlobalHelper.saveUserLoggedInData(
              isUserLoggedIn: true,
              userName: _fullNameController.text,
              userEmail: _emailController.text);

          GlobalHelper.goToReplacementScreen(context, HomePage());
        } else {
          setState(() {
            _isLoadingRegister = false;
          });
          GlobalWidget.showSnackBar(
              context: context,
              backgroundColor: Colors.red,
              message: value.toString());
        }
      });
    }
  }
}
