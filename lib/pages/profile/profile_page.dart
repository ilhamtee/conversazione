import 'package:flutter/material.dart';


class ProfilePage extends StatefulWidget {
  final String fullName;
  final String email;
  const ProfilePage({Key? key, required this.fullName, required this.email}) : super(key: key);

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      padding: const EdgeInsets.symmetric(horizontal: 60),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget> [
          Icon(Icons.account_circle, size: 200, color: Colors.grey[500],),
          const SizedBox(height: 15),
          Row(
            children: [
              const Expanded(
                flex: 1,
                child: Text('Full Name ', style: TextStyle(
                  fontSize: 17,
                  fontWeight: FontWeight.w500
                ),),
              ),
              Expanded(
                flex: 1,
                child: Text(widget.fullName, style: const TextStyle(
                    fontSize: 17,
                    fontWeight: FontWeight.w500
                ),textAlign: TextAlign.right),
              ),
            ],
          ),
          const SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              const Expanded(
                flex: 1,
                child: Text('Email', style: TextStyle(
                    fontSize: 17,
                    fontWeight: FontWeight.w500
                ),),
              ),
              Expanded(
                flex: 1,
                child: Text(widget.email, style: const TextStyle(
                    fontSize: 17,
                    fontWeight: FontWeight.w500
                ), textAlign: TextAlign.right,),
              ),
            ],
          ),
          const SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: const [
              Expanded(
                flex: 1,
                child: Text('Status', style: TextStyle(
                    fontSize: 17,
                    fontWeight: FontWeight.w500
                ),),
              ),
              Expanded(
                flex: 1,
                child: Text('ACTIVE', style: TextStyle(
                    fontSize: 17,
                    fontWeight: FontWeight.w500,
                  color: Colors.green,
                ), textAlign: TextAlign.right,),
              ),
            ],
          )
        ],
      ),
    );
  }
}
