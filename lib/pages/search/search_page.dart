import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_chat_app/configs/style_config.dart';
import 'package:firebase_chat_app/helpers/global_helper.dart';
import 'package:firebase_chat_app/pages/search/widgets/list_search_group.dart';
import 'package:firebase_chat_app/services/database_service.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SearchPage extends StatefulWidget {
  const SearchPage({Key? key}) : super(key: key);

  @override
  State<SearchPage> createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  final TextEditingController _searchController = TextEditingController();
  bool _isLoadingSearchGroup = false;

  QuerySnapshot? _searchSnapshot;

  @override
  void dispose() {
    super.dispose();
    _searchController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: StyleConfig.primaryColor,
        title: const Text('Search'),
        centerTitle: true,
        elevation: 0,
      ),
      body: Column(
        children: [
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
            color: StyleConfig.primaryColor,
            child: Row(
              children: [
                Expanded(
                    child: TextField(
                  controller: _searchController,
                  style: const TextStyle(color: Colors.white),
                  decoration: const InputDecoration(
                      border: InputBorder.none,
                      hintText: 'Search a group ...',
                      hintStyle: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.w400)),
                )),
                GestureDetector(
                  onTap: () => _searchGroup(),
                  child: Container(
                    height: 40,
                    width: 40,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.white.withOpacity(0.15)),
                    alignment: Alignment.center,
                    child: const Icon(
                      Icons.search,
                      color: Colors.white,
                    ),
                  ),
                )
              ],
            ),
          ),
          _isLoadingSearchGroup
              ? const Expanded(
                  child: Center(
                    child: CircularProgressIndicator(
                      backgroundColor: StyleConfig.primaryColor,
                      color: Colors.white,
                    ),
                  ),
                )
              : !_isLoadingSearchGroup && _searchSnapshot != null
                  ? ListSearchGroup(searchSnapshot: _searchSnapshot)
                  : Expanded(
                      child: Center(
                      child: Image.asset(
                        'assets/images/search_bg.jpg',
                        width: 300,
                        height: 300,
                        fit: BoxFit.contain,
                      ),
                    ))
        ],
      ),
    );
  }

  void _searchGroup() {
    if (_searchController.text.isNotEmpty) {
      setState(() {
        _isLoadingSearchGroup = true;
      });
      DatabaseService()
          .searchGroupByName(_searchController.text)
          .then((snapshot) {
        setState(() {
          _searchSnapshot = snapshot;
          _isLoadingSearchGroup = false;
        });
      });
    }
  }
}
