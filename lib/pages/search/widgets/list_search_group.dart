import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_chat_app/helpers/global_helper.dart';
import 'package:firebase_chat_app/pages/search/widgets/item_search_group.dart';
import 'package:flutter/material.dart';

class ListSearchGroup extends StatelessWidget {
  final QuerySnapshot? searchSnapshot;
  const ListSearchGroup({Key? key, required this.searchSnapshot})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return searchSnapshot != null && searchSnapshot!.docs.isNotEmpty
        ? ListView.builder(
            shrinkWrap: true,
            padding: const EdgeInsets.symmetric(vertical: 10),
            itemCount: searchSnapshot!.docs.length,
            itemBuilder: (context, index) {
              final resultGroup = searchSnapshot!.docs[index];
              return ItemSearchGroup(
                  groupId: resultGroup['groupId'],
                  groupName: resultGroup['groupName'],
                  groupAdmin: GlobalHelper.getName(resultGroup['admin']));
            })
        : searchSnapshot != null && searchSnapshot!.docs.isEmpty
            ? Expanded(
                child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      'assets/images/group_not_found.jpg',
                      width: 300,
                      height: 300,
                      fit: BoxFit.contain,
                    ),
                    const Text(
                      "Group not found! \nMaybe the group you are looking for "
                      "doesn't exist \nor Try entering another group name",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.italic),
                    ),
                  ],
                ),
              ))
            : const SizedBox();
  }
}
