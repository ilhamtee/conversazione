import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_chat_app/configs/style_config.dart';
import 'package:firebase_chat_app/helpers/global_helper.dart';
import 'package:firebase_chat_app/helpers/global_widget.dart';
import 'package:firebase_chat_app/pages/chat/chat_page.dart';
import 'package:firebase_chat_app/services/database_service.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ItemSearchGroup extends StatefulWidget {
  final String groupId;
  final String groupName;
  final String groupAdmin;
  const ItemSearchGroup(
      {Key? key,
      required this.groupId,
      required this.groupName,
      required this.groupAdmin})
      : super(key: key);

  @override
  State<ItemSearchGroup> createState() => _ItemSearchGroupState();
}

class _ItemSearchGroupState extends State<ItemSearchGroup> {
  bool _isUserJoined = false;
  String _groupAdminName = '';
  String _userName = '';

  void checkUserJoinAndGroupAdmin() async {
    bool isUserJoin = false;
    String adminName = '';
    isUserJoin =
        await DatabaseService(userId: FirebaseAuth.instance.currentUser!.uid)
            .isUserJoined(widget.groupId, widget.groupName);
    adminName =
        await DatabaseService(userId: FirebaseAuth.instance.currentUser!.uid)
            .checkGroupAdmin(widget.groupAdmin);
    final sharedPreference = await SharedPreferences.getInstance();

    setState(() {
      _isUserJoined = isUserJoin;
      _groupAdminName = adminName;
      _userName = sharedPreference.getString(GlobalHelper.userNameKey)!;
    });
  }

  @override
  void initState() {
    super.initState();
    checkUserJoinAndGroupAdmin();
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
        contentPadding: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
        leading: CircleAvatar(
          backgroundColor: StyleConfig.primaryColor,
          radius: 30,
          child: Text(
            widget.groupName.substring(0, 1).toUpperCase(),
            style: const TextStyle(
                fontSize: 20, color: Colors.white, fontWeight: FontWeight.w400),
          ),
        ),
        title: Text(
          widget.groupName,
          style: const TextStyle(fontWeight: FontWeight.w600, fontSize: 18),
        ),
        subtitle: Text(
          'Admin : $_groupAdminName',
          style: const TextStyle(fontSize: 15),
        ),
        trailing: ElevatedButton(
          style: ElevatedButton.styleFrom(
              backgroundColor:
                  _isUserJoined ? Colors.grey : StyleConfig.primaryColor,
              padding: const EdgeInsets.symmetric(horizontal: 25),
              elevation: _isUserJoined ? 0 : 3),
          onPressed: () async {
            if (_isUserJoined) {
              showDialog(
                  context: context,
                  builder: (context) {
                    return AlertDialog(
                      title: const Text('Leave Group'),
                      contentPadding: const EdgeInsets.symmetric(
                          vertical: 20, horizontal: 20),
                      content: const Text(
                        'Are you sure want to leave the group?',
                        style: TextStyle(fontSize: 18),
                      ),
                      actions: [
                        GestureDetector(
                          onTap: () => Navigator.pop(context),
                          child: const Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: 5, vertical: 5),
                            child: Text(
                              'NO',
                              style: TextStyle(
                                  color: StyleConfig.primaryColor,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w500),
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () async {
                            await DatabaseService(
                                    userId:
                                        FirebaseAuth.instance.currentUser!.uid)
                                .handleJoinGroup(widget.groupId, _userName,
                                    widget.groupName);
                            GlobalWidget.showSnackBar(
                                context: context,
                                backgroundColor: Colors.red,
                                message:
                                    'You have left the ${widget.groupName} '
                                    'group');
                            setState(() {
                              _isUserJoined = !_isUserJoined;
                            });
                            Navigator.pop(context);
                          },
                          child: const Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: 5, vertical: 5),
                            child: Text(
                              'YES',
                              style: TextStyle(
                                  color: StyleConfig.primaryColor,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400),
                            ),
                          ),
                        ),
                      ],
                    );
                  });
            } else {
              await DatabaseService(
                      userId: FirebaseAuth.instance.currentUser!.uid)
                  .handleJoinGroup(widget.groupId, _userName, widget.groupName);
              setState(() {
                _isUserJoined = !_isUserJoined;
              });
              GlobalWidget.showSnackBar(
                  context: context,
                  backgroundColor: Colors.green,
                  message: 'Successfully joined the group');
              Future.delayed(const Duration(milliseconds: 1800), () {
                GlobalHelper.goToNextScreen(
                    context,
                    ChatPage(
                        groupId: widget.groupId,
                        groupName: widget.groupName,
                        userName: _userName));
              });
            }
          },
          child: Text(_isUserJoined ? 'JOINED' : 'JOIN'),
        ));
  }
}
