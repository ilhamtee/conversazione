# Conversazione (Firebase Chat App)

Example of flutter integration with firebase.
All Asset Images are from : https://www.freepik.com/
Author : stories

## PREVIEW UI
### Splash Screen
![Splash SCREEN](assets/preview_ui/splash.png)

###
### Login Screen
![Login SCREEN](assets/preview_ui/login.png)

###
### Register Screen
![Register SCREEN](assets/preview_ui/register.png)

###
### Home Screen
![Home SCREEN](assets/preview_ui/groups_page.png)
![Create Group](assets/preview_ui/create group.png)
![Confirm Logout SCREEN](assets/preview_ui/confirm_logout.png)

###
### Search Screen
![Search SCREEN](assets/preview_ui/search_group_empty.png)
![Search SCREEN](assets/preview_ui/search_group.png)
![Search SCREEN](assets/preview_ui/search_group_not_found.png)

###
### Chat Screen
![Chat SCREEN](assets/preview_ui/chat_page_empty.png)
![Chat SCREEN](assets/preview_ui/chat_page.png)

###
### Profile Screen
![Profile SCREEN](assets/preview_ui/profile_page.png)




